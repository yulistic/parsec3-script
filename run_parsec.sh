#!/bin/bash
# parsecmgmt -a run -p blackscholes -n 64 -i native

# ./bin/parsecmgmt -a run -p swaptions -n 32 -i native

# long run.
# time /home/yulistic/srcs/parsec-3.0/pkgs/apps/swaptions/inst/amd64-linux.gcc/bin/swaptions -ns 128 -sm 10000000 -nt 64
# time /home/yulistic/srcs/parsec-3.0/pkgs/apps/swaptions/inst/amd64-linux.gcc/bin/swaptions -ns 128 -sm 10000000 -nt 64

#time /home/yulistic/srcs/parsec-3.0/pkgs/apps/ferret/inst/amd64-linux.gcc/bin/ferret corel lsh queries 50 20 64 output.txt
# time /home/yulistic/srcs/parsec-3.0/pkgs/apps/ferret/inst/amd64-linux.gcc/bin/ferret corel lsh queries 50 30 64 output.txt
# parsecmgmt -a run -p ferret -n 32 -i native

if [ $1 == "short" ]; then
    echo "Run ferret short"
    # To measure Ferret throughput (./run_iobench_tput_single.sh parsec)
    (cd /home/yulistic/srcs/parsec-3.0/pkgs/apps/ferret/run; time numactl -N0 -m0 /home/yulistic/srcs/parsec-3.0/pkgs/apps/ferret/inst/amd64-linux.gcc/bin/ferret corel lsh queries 50 20 16 output.txt)
else
    echo "Run ferret long"
    # To measure iobench throughput (./run_iobench_tput_single.sh cpu)
    (cd /home/yulistic/srcs/parsec-3.0/pkgs/apps/ferret/run; time numactl -N0 -m0 /home/yulistic/srcs/parsec-3.0/pkgs/apps/ferret/inst/amd64-linux.gcc/bin/ferret corel lsh queries 200 80 16 output.txt)

fi

